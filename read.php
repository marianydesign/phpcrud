<?php
include 'functions.php';
// Connect to MySQL database
$pdo = pdo_connect_mysql();
// Get the page via GET request (URL param: page), if non exists default the page to 1
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
// Number of records to show on each page
$records_per_page = isset($_GET['records_per_page']) && (is_numeric($_GET['records_per_page']) || $_GET['records_per_page'] == 'all') ? $_GET['records_per_page'] : 5;
// Which columns the users can order by, add/remove from the array below.
$order_by_list = array('id','name','email','phone','title','created');
// Order by which column if specified (default to id)
$order_by = isset($_GET['order_by']) && in_array($_GET['order_by'], $order_by_list) ? $_GET['order_by'] : 'id';
// Sort by ascending or descending if specified (default to ASC)
$order_sort = isset($_GET['order_sort']) && $_GET['order_sort'] == 'DESC' ? 'DESC' : 'ASC';
if (isset($_GET['search'])) {
	if ($records_per_page == 'all') {
		// SQL statement to get all contacts with search query
		$stmt = $pdo->prepare('SELECT * FROM contacts
							   WHERE id LIKE :search_query
								  OR name LIKE :search_query
								  OR email LIKE :search_query
								  OR phone LIKE :search_query
								  OR title LIKE :search_query
								  OR created LIKE :search_query
								ORDER BY ' . $order_by . ' ' . $order_sort);
		$stmt->bindValue(':search_query', '%' . $_GET['search'] . '%');
	} else {
		// Custom search, if the user entered text in the search box and pressed enter...
		// The below query will search in every column until it's fount a match, feel free to remove a field if you don't want to search it.
		$stmt = $pdo->prepare('SELECT * FROM contacts
							   WHERE id LIKE :search_query
								  OR name LIKE :search_query
								  OR email LIKE :search_query
								  OR phone LIKE :search_query
								  OR title LIKE :search_query
								  OR created LIKE :search_query
								ORDER BY ' . $order_by . ' ' . $order_sort . '
								LIMIT :current_page, :record_per_page');
		// The percentages are added each side of the search query so we can find a match in the column value.
		$stmt->bindValue(':search_query', '%' . $_GET['search'] . '%');
		$stmt->bindValue(':current_page', ($page-1)*(int)$records_per_page, PDO::PARAM_INT);
		$stmt->bindValue(':record_per_page', (int)$records_per_page, PDO::PARAM_INT);
	}
} else {
	if ($records_per_page == 'all') {
		// SQL statement to get all contacts
		$stmt = $pdo->prepare('SELECT * FROM contacts ORDER BY ' . $order_by . ' ' . $order_sort);
	} else {
		// Prepare the SQL statement and get records from our contacts table, LIMIT will determine the page
		$stmt = $pdo->prepare('SELECT * FROM contacts ORDER BY ' . $order_by . ' ' . $order_sort . ' LIMIT :current_page, :record_per_page');
		$stmt->bindValue(':current_page', ($page-1)*(int)$records_per_page, PDO::PARAM_INT);
		$stmt->bindValue(':record_per_page', (int)$records_per_page, PDO::PARAM_INT);
	}
}
// The above queries are ordered by id, you can change this if you want to order by another column, such as "name"
$stmt->execute();
// Fetch the records so we can display them in our template.
$contacts = $stmt->fetchAll(PDO::FETCH_ASSOC);
// Get the total number of contacts, this is so we can determine whether there should be a next and previous button
if (isset($_GET['search'])) {
	$stmt = $pdo->prepare('SELECT COUNT(*) FROM contacts
						   WHERE id LIKE :search_query
							  OR name LIKE :search_query
							  OR email LIKE :search_query
							  OR phone LIKE :search_query
							  OR title LIKE :search_query
							  OR created LIKE :search_query');
	$stmt->bindValue(':search_query', '%' . $_GET['search'] . '%');
	$stmt->execute();
	$num_contacts = $stmt->fetchColumn();
} else {
	$num_contacts = $pdo->query('SELECT COUNT(*) FROM contacts')->fetchColumn();
}
?>

<?=template_header('Read')?>

<div class="content read">
	<h2>Read Contacts</h2>
	<div class="top">
		<a href="create.php">Create Contact</a>
		<form action="read.php" method="get">
			<input type="text" name="search" placeholder="Search..." value="<?=isset($_GET['search']) ? htmlentities($_GET['search'], ENT_QUOTES) : ''?>">
		</form>
	</div>
	<table>
        <thead>
            <tr>
                <td>
					<a href="read.php?page=1&records_per_page=<?=$records_per_page?>&order_by=id>&order_sort=<?=$order_sort == 'ASC' ? 'DESC' : 'ASC'?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">
						#
						<?php if ($order_by == 'id'): ?>
						<i class="fas fa-long-arrow-alt-<?=str_replace(array('ASC', 'DESC'), array('up', 'down'), $order_sort)?>"></i>
						<?php endif; ?>
					</a>
				</td>
                <td>
					<a href="read.php?page=1&records_per_page=<?=$records_per_page?>&order_by=name&order_sort=<?=$order_sort == 'ASC' ? 'DESC' : 'ASC'?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">
						Name
						<?php if ($order_by == 'name'): ?>
						<i class="fas fa-long-arrow-alt-<?=str_replace(array('ASC', 'DESC'), array('up', 'down'), $order_sort)?>"></i>
						<?php endif; ?>
					</a>
				</td>
                <td>
					<a href="read.php?page=1&records_per_page=<?=$records_per_page?>&order_by=email&order_sort=<?=$order_sort == 'ASC' ? 'DESC' : 'ASC'?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">
						Email
						<?php if ($order_by == 'email'): ?>
						<i class="fas fa-long-arrow-alt-<?=str_replace(array('ASC', 'DESC'), array('up', 'down'), $order_sort)?>"></i>
						<?php endif; ?>
					</a>
                </td>
                <td>
					<a href="read.php?page=1&records_per_page=<?=$records_per_page?>&order_by=phone&order_sort=<?=$order_sort == 'ASC' ? 'DESC' : 'ASC'?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">
						Phone
						<?php if ($order_by == 'phone'): ?>
						<i class="fas fa-long-arrow-alt-<?=str_replace(array('ASC', 'DESC'), array('up', 'down'), $order_sort)?>"></i>
						<?php endif; ?>
					</a>
                </td>
                <td>
					<a href="read.php?page=1&records_per_page=<?=$records_per_page?>&order_by=title&order_sort=<?=$order_sort == 'ASC' ? 'DESC' : 'ASC'?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">
						Title
						<?php if ($order_by == 'title'): ?>
						<i class="fas fa-long-arrow-alt-<?=str_replace(array('ASC', 'DESC'), array('up', 'down'), $order_sort)?>"></i>
						<?php endif; ?>
					</a>
                </td>
                <td>
					<a href="read.php?page=1&records_per_page=<?=$records_per_page?>&order_by=created&order_sort=<?=$order_sort == 'ASC' ? 'DESC' : 'ASC'?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">
						Created
						<?php if ($order_by == 'created'): ?>
						<i class="fas fa-long-arrow-alt-<?=str_replace(array('ASC', 'DESC'), array('up', 'down'), $order_sort)?>"></i>
						<?php endif; ?>
					</a>
                </td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contacts as $contact): ?>
            <tr>
                <td><?=$contact['id']?></td>
                <td><?=$contact['name']?></td>
                <td><?=$contact['email']?></td>
                <td><?=$contact['phone']?></td>
                <td><?=$contact['title']?></td>
                <td><?=$contact['created']?></td>
                <td class="actions">
                    <a href="update.php?id=<?=$contact['id']?>" class="edit"><i class="fas fa-pen fa-xs"></i></a>
                    <a href="delete.php?id=<?=$contact['id']?>" class="trash"><i class="fas fa-trash fa-xs"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
	<div class="bot">
		<div class="records-per-page">
			<a href="read.php?page=1&records_per_page=5&order_by=<?=$order_by?>&order_sort=<?=$order_sort?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">5</a>
			<a href="read.php?page=1&records_per_page=10&order_by=<?=$order_by?>&order_sort=<?=$order_sort?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">10</a>
			<a href="read.php?page=1&records_per_page=20&order_by=<?=$order_by?>&order_sort=<?=$order_sort?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">20</a>
			<a href="read.php?page=1&records_per_page=50&order_by=<?=$order_by?>&order_sort=<?=$order_sort?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">50</a>
			<a href="read.php?page=1&records_per_page=100&order_by=<?=$order_by?>&order_sort=<?=$order_sort?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">100</a>
			<a href="read.php?page=1&records_per_page=all&order_by=<?=$order_by?>&order_sort=<?=$order_sort?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">all</a>
		</div>
		<div class="pagination">
			<?php if ($page > 1): ?>
			<a href="read.php?page=<?=$page-1?>&records_per_page=<?=$records_per_page?>&order_by=<?=$order_by?>&order_sort=<?=$order_sort?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">
				<i class="fas fa-angle-double-left fa-sm"></i>
			</a>
			<?php endif; ?>
			<div>Page <?=$page?></div>
			<?php if ($records_per_page != 'all' && $page*$records_per_page < $num_contacts): ?>
			<a href="read.php?page=<?=$page+1?>&records_per_page=<?=$records_per_page?>&order_by=<?=$order_by?>&order_sort=<?=$order_sort?><?=isset($_GET['search']) ? '&search=' . htmlentities($_GET['search'], ENT_QUOTES) : ''?>">
				<i class="fas fa-angle-double-right fa-sm"></i>
			</a>
			<?php endif; ?>
		</div>
	</div>
</div>

<?=template_footer()?>
